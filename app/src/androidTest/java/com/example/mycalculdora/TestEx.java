package com.example.mycalculdora;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;


import com.example.mycalculdora.controllers.MainActivity;



import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class TestEx {

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class, true,
                    true);

    @Test
    public void ErrorAlturaTest() {
        onView(withId(R.id.btnBMI)).perform(click());
        onView(withId(R.id.etAltura)).perform(typeText(""),closeSoftKeyboard());
        onView(withId(R.id.etKilos)).perform(typeText("80"),closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withText("Falta l'altura")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void ErrorEuroTest() {
        onView(withId(R.id.btnEuro)).perform(click());
        onView(withId(R.id.etCantidad)).perform(typeText(""),closeSoftKeyboard());
        onView(withId(R.id.btnResult)).perform(click());
        onView(withText("Falta la quantitat")).inRoot(withDecorView(not(activityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void SuccessTest() {

        onView(withId(R.id.btnBMI)).perform(click());
        onView(withId(R.id.etAltura)).perform(typeText("165"),closeSoftKeyboard());
        onView(withId(R.id.etKilos)).perform(typeText("80"),closeSoftKeyboard());
        onView(withId(R.id.btnCalcular)).perform(click());
        onView(withId(R.id.tvCalculat)).check(matches(withText("Sobrepès")));
        onView(withId(R.id.tvresult2)).check(matches(withText("29.38")));

    }

}
