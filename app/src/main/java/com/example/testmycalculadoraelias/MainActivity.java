package com.example.testmycalculadoraelias;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.mycalculdora.R;
import com.example.mycalculdora.controllers.CalculadoraBMI;
import com.example.mycalculdora.controllers.CalculadoraEuroDolar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConstraintLayout constraintLayout = findViewById(R.id.ConstraintLayoutMain);
        constraintLayout.setBackgroundColor(Color.rgb(125,125,125));
        Button btnBMI = findViewById(R.id.btnBMI);
        Button btnEuro = findViewById(R.id.btnEuro);
        btnBMI.setOnClickListener((View.OnClickListener) this);
        btnEuro.setOnClickListener((View.OnClickListener) this);
    }

    public void onClick(View v) {

        if (v.getId()==R.id.btnBMI){
            Intent intentBMI = new Intent (this, CalculadoraBMI.class);
            startActivity(intentBMI);
        }
        if(v.getId() == R.id.btnEuro){
            Intent intentEuro = new Intent (this, CalculadoraEuroDolar.class);
            startActivity(intentEuro);
        }
    }
}
